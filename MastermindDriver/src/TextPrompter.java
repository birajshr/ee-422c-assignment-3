import java.util.*;

public class TextPrompter
{
	private String prompt;
	private boolean difficulty;
	
	public TextPrompter()
	{
		prompt = "NewGame";
	}

	public void defaultIntro(boolean showCode)
	{
		difficulty = showCode;
		Code introCode = new Code();
		Board introBoard = new Board();
		Pegs introPegs = new Pegs();
		String intro = "Welcome to Mastermind.\n" +
				"This is a text version of the classic board game Mastermind.\n" +
				"The computer will think of a secret code.\n" +
				"The code consists of "+ introCode.getLength() + " colored pegs.\n" +
				"The pegs may be one of " + introPegs.getPegNum() + " colors:" +
				" blue, green , orange, purple, red, or yellow.\n" +
				"A color may appear more than once in the code.\n\n" +
				
				"You try to guess what colored pegs are in the code and what order they are in\n" +
				"After making a guess the result will be displayed.\n" +
				"A result consists of a black peg for each peg you have exactly " +
				"correct (color and position) in your guess.\n" +
				"For each peg in the guess that is the correct color, but is out of position, you get a white peg.\n\n" +
				
				"Only the first letter of the color is displayed. B for Blue, R for Red, and so forth.\n" +
				"When entering guesses you only need to enter the first character of the color as a capital letter.\n\n" +

				"You have " + introBoard.getColumns() + " to guess the answer or you lose the game.\n\n";

		System.out.println(intro);
		
		defaultGame(difficulty);
	}
	
	private void defaultGame(boolean difficulty)
	{
		Scanner sc = new Scanner(System.in);
		Code gamecode = new Code();
		System.out.println("Generating secret code ....");
		if (difficulty)
		{
			System.out.println("The secret code is " + gamecode.getCode());
		}
		Board gameboard = new Board();
		int rounds = 0;
		boolean isCodeSame = false;
		String firstRow = gameboard.getRow(rounds) + " Secret Code";

		gameboard.changeRow(rounds, firstRow);
		
		
		while ( (rounds < gameboard.getColumns()) && !isCodeSame )
		{
			System.out.println("You have " + (gameboard.getColumns() - rounds) + " guesses left.\n");
			System.out.println("What is your guess?");
			System.out.println("Type in the characters for your guess and press enter.");
			System.out.print("Enter guess: ");
			String userGuess = sc.next();
			System.out.println();
			
			if (difficulty)
			{
				System.out.println("The secret code is " + gamecode.getCode() + "\n");
			}
			
			while((userGuess == (null)) || (userGuess.length() != gameboard.getNumPegs()) || !(Pegs.isValidCode(userGuess))  )
			{
				System.out.println("What is your guess?");
				System.out.println("Type in the characters for your guess and press enter.");
				System.out.print("Enter guess: ");
				userGuess = sc.next();
				System.out.println();
			}
			
			rounds++;
			
			String row = userGuess +" Result:"+ gamecode.checkCode(gamecode, userGuess);
			gameboard.changeRow(rounds, row);
			
			
			isCodeSame = gamecode.isCodeSame(userGuess);
			
			if (isCodeSame)
			{
				firstRow = gamecode.getCode();
				gameboard.changeRow(gameboard.getColumns() - gameboard.getColumns(), firstRow);
			}
			
			System.out.print(gameboard.toString());

		}
		
		if (isCodeSame)
		{
			System.out.println("You solved the puzzle! Good job.");
			
		}
		else
			System.out.println("You did not solve the puzzle. Too bad.");
		
		continueGame(difficulty);
	}
	
	private void continueGame(boolean difficulty)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Y for another game or anything else to quit: ");
		String userchoice = sc.next();
		if (userchoice.equalsIgnoreCase("Y"))
			defaultGame(difficulty);
	}
	
}