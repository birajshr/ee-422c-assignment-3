
import java.util.*;

public class Code
{
	private ArrayList<Character> secretCode;
	private int CODE_SIZE;
	private int NUM_PEGS;
	
	public Code()
	{
		Board gameboard = new Board();
		Pegs colors = new Pegs();
		
		CODE_SIZE = gameboard.getNumPegs();
		NUM_PEGS = colors.getPegNum();
		
		secretCode = new ArrayList<Character>();
		
		int randomNum;
		
		for (int i = 0; i < CODE_SIZE; i++)
		{
			Random rand = new Random();
			randomNum = rand.nextInt(NUM_PEGS);
			secretCode.add(colors.getPeg(randomNum));
		}
	}
	
	public Code (Code paramCode)
	{
		secretCode = new ArrayList<Character>();
		for (int i = 0; i < paramCode.getLength(); i++)
			secretCode.add(paramCode.getColor(i));
	}
	
	public Code (String strCode)
	{
		secretCode = new ArrayList<Character>();
		for (int i = 0; i < strCode.length(); i++)
			secretCode.add(strCode.charAt(i));
	}
	
	public int getLength()
	{
		return secretCode.size();
	}
	
	public char getColor(int index)
	{
		return secretCode.get(index);
	}
	
	public String getCode()
	{
		String code = "";
		for (int i = 0; i < this.getLength(); i++)
			code += secretCode.get(i);
		return code;
	}
	
	public void modifyCode(int index)
	{
		secretCode.set(index, '-');
	}
	
	public boolean isCodeSame(String userGuess)
	{
		return this.getCode().equals(userGuess);
	}
	
	public String checkCode(Code gameCode, String userGuess)
	{
		Code secretCode = new Code(gameCode); //this.getCode());
		boolean[] helper = new boolean[userGuess.length()];
		String result = "";
		for(int i = 0; i < userGuess.length(); i++)
		{
			if ((!helper[i]) && (userGuess.charAt(i) == secretCode.getColor(i)))//tempCode.getColor(i))) 
			{
				result += " Black";
				helper[i]=true;
				secretCode.modifyCode(i);
			}
		}
		
		for(int i = 0; i < userGuess.length(); i++)
			for (int j = 0; j < userGuess.length(); j++)
			{
				if ((!helper[i]) && (userGuess.charAt(i) == secretCode.getColor(j)))//tempCode.getColor(j)))
				{
					result += " White";
					helper[i]=true;
					secretCode.modifyCode(j);
				}
			}
		if (result.equalsIgnoreCase(""))
		{
			result = " No pegs";
		}
		
		return result;
	}
	
}
