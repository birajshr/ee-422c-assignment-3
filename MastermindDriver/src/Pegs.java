import java.util.*;
public class Pegs
{
	private static ArrayList<Character> pegArray;
	private int NUM_PEGS = 6;
	
	private static final char BLUE = 'B';
	private static final char GREEN = 'G';
	private static final char ORANGE = 'O';
	private static final char PURPLE = 'P';
	private static final char RED = 'R';
	private static final char YELLOW = 'Y';
	
	public Pegs()
	{
		pegArray = new ArrayList<Character>();
		
		for(int i = 0; i < NUM_PEGS; i++)
		{
			switch (i+1)
			{
				case 1: pegArray.add(BLUE);
						break;
				case 2: pegArray.add(GREEN);
						break;
				case 3: pegArray.add(ORANGE);
						break;
				case 4: pegArray.add(PURPLE);
						break;
				case 5: pegArray.add(RED);
						break;
				case 6: pegArray.add(YELLOW);
						break;
			}
		}
	}
	
	public char getPeg(int idx)
	{
		return pegArray.get(idx);
	}
	
	public int getPegNum()
	{
		return pegArray.size();
	}
	
	public static boolean isValidCode(String userGuess)
	{
		int containNum = -1;
		for (int i = 0; i < userGuess.length(); i++)
		{
			if (!(pegArray.contains(userGuess.charAt(i))))
			{
				containNum++;
			}
		}
		
		return containNum == -1;
	}
	
}