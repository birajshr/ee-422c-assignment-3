
import java.util.*;
public class Board
{
	private ArrayList<String> gameBoard;
	private int numPegs = 4;
	private int columns = 12;
	//columns is the same thing as the number of guesses
	
	public Board()
	{
		gameBoard = new ArrayList<String>();

		String defaultDots = "";
		for (int i = 0; i < numPegs; i++)
		{
			defaultDots += ".";
		}
		for(int i = 0; i < columns + 1; i++)
		{
			gameBoard.add(defaultDots);
		}
	}
	
	public int getColumns()
	{
		return columns;
	}
	
	public int getNumPegs()
	{
		return numPegs;
	}
	
	public String getRow(int index)
	{
		return gameBoard.get(index);
	}
	
	public String toString()
	{
		String str = "";
		for (int i = 0; i < gameBoard.size(); i++)
		{
			str += gameBoard.get(i) + "\n";
		}
		return str;
	}
	
	public void changeRow(int index, String userGuess)
	{
		gameBoard.set(index, userGuess);
	}
	
}