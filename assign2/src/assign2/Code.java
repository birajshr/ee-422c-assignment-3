package assign2;

import java.util.Random;

/**
Code class that holds game codes
Solves EE422C programming assignment #2
@author Biraj Shrestha, Eric Van Dyk 
*/
public class Code extends Game
{
	
	StringBuilder code = new StringBuilder();
	int length;
	
	public Code()
	{
		super();
		length = 4;
	}
		
	/**
	   Creates random game code
	   @param Colors class of allowed colors, boolean if test Mode
	   @return void
	*/
	public void randomize(Colors allowed, boolean testMode)
	{		
		Colors randColor = allowed;
		Random rand = new Random();
		int base;
		
		System.out.println("\n\nGenerating secret code ....\n"); 
		
		for(int i = 0; i < length; i++)
		{
			base = rand.nextInt(randColor.numColors);
			code.append(randColor.getColorAt(base));
		}
		
		if(testMode == true)
		{
			System.out.println(code);
		}		
	}
	
	/**
	   Compares guess with game code
	   @param Code class to be checked against
	   @return boolean value if codes are the same
	*/
	public boolean compareGuess(Code currGuess)
	{
		
		boolean isEqual = true;
		String a, b;
		StringBuilder code = new StringBuilder();
		StringBuilder guess = new StringBuilder();
		for(int i=0;i<this.length;i++)
		{
			a = this.valAt(i);
			b = currGuess.valAt(i);
			if(a.equals(b) == false)
				isEqual=false;
		}

		if(isEqual == true)
		{
			super.setBlackPegs(4);
			return true;
		}
		else
		{
			code.replace(0, this.length, this.code.toString());
			guess.replace(0, currGuess.length, currGuess.code.toString());
			for(int x = 0; x < this.length; x++)
			{
				if(code.charAt(x) == guess.charAt(x))
				{
					super.blackPegs += 1;
					code.setCharAt(x, '*');
					guess.setCharAt(x, '*');
				}
			}
			
			for(int y = 0; y < this.length; y++)
			{
				if(code.charAt(y) != '*')
				{
					for(int z = 0; z< guess.length(); z++)
					{
						if(guess.charAt(z) != '*')
						{
							if(code.charAt(y) == guess.charAt(z))
							{
								super.whitePegs += 1;
								code.setCharAt(y, '*');
								guess.setCharAt(z, '*');
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	   Sets code to certain value
	   @param Stringbuilder value of code
	   @return void
	*/
	public void setCode(StringBuilder in)
	{
		code = in;
		this.setLength(code.length());
	}
	
	/**
	   Returns value of piece of Code
	   @param int i of index
	   @return String value of code[index]
	*/
	public String valAt(int i)
	{
		return this.code.substring(i,i+1);
	}
		
	/**
	   Sets length of code
	   @param int s of length
	   @return void
	*/
	public void setLength(int s)
	{
		this.length = s;
	}
	
	/**
	   gets length of code
	   @param void
	   @return code length
	*/
	public int getLength()
	{
		return this.length;
	}

}
