package assign2;
import java.util.*;

/**
Color class that holds all available colors possible
Solves EE422C programming assignment #2
@author Biraj Shrestha, Eric Van Dyk 
*/

public class Colors{
	
	ArrayList<String> availableColors = new ArrayList<String>();
	int numColors;
	public Colors()
	{
		numColors = 6;
		availableColors.add("R");
		availableColors.add("O");
		availableColors.add("Y");
		availableColors.add("G");
		availableColors.add("B");
		availableColors.add("P");
	}
	
	/**
	   Adds to available colors
	   @param String value of color
	   @return void
	*/
	public void addColors(String color)
	{
		availableColors.add(color);
		numColors++;
	}
	
	/**
	   returns length
	   @param void
	   @return int value of length
	*/
	public int length()
	{
		return numColors;
	}
	
	/**
	   Gets color at a position
	   @param int position to get val at
	   @return String of color
	*/
	public String getColorAt(int position)
	{
		return availableColors.get(position);
	}
	
	/**
	   Checks if color is included in available colors
	   @param Code checker to check against, and int i to check position
	   @return boolean value of if it is included
	*/
	public boolean included(Code checker, int i)
	{
		if(availableColors.contains(checker.valAt(i)))
		{
			return true;
		}
		else return false;
	}
}
