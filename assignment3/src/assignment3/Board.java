package assignment3;
import java.util.Arrays;
import java.util.*;
import javax.swing.JOptionPane;

/**
Board class that runs each turn of the game
Solves EE422C programming assignment #2
@author Biraj Shrestha, Chris Hogue 
*/
public class Board 
{
	ArrayList<Peg> pegs= new ArrayList<Peg>();
	private boolean debug;
	private int numTurns;
	private int numPegs;
	private Code secretCode;
	Color[] gameColors = new Color[7];
	
	/**
	 * Board constructor. Initializes the variable attributes of the Board class 
	 * 
	 * @param debug - if true, the secret code is revealed during each turn.
	 * 		  numTurns - the total number of turns for each game.
	 *        numPegs - the total number of pegs for each turn.
	 *    
	 */
	public Board( boolean debug, int numTurns, int numPegs)
	{
		this.debug = debug;
		this.numTurns = numTurns;
		this.numPegs = numPegs;
		
		gameColors[0] = new Color('B');
		gameColors[1] = new Color('G');
		gameColors[2] = new Color('O');
		gameColors[3] = new Color('P');
		gameColors[4] = new Color('R');
		gameColors[5] = new Color('Y');
		gameColors[6] = new Color('M');
			
		secretCode = new Code(numPegs, this);
		
		
	}
		
	/**
	 * turn Method. Cycles through each turn getting input and placing pegs 
	 * 
	 * @param p1 - Player playing the game
	 * 		  turn - current turn
	 *    
	 */
	public void turn( Player p1, int turn)
	{
		Code playerCode;
		do
		{
		 playerCode = p1.getInput(turn , this);
		}
		while (!playerCode.valid(numPegs, p1));
		
		p1.winner = placePegs(playerCode, p1, turn);
		
	}
	
	/**
	 * placePegs Method. Checks the Player code against 
	 * secret code for number of black and white pegs
	 * 
	 * @param p1 - Player playing the game
	 * 		  turn - current turn
	 * 		  playerCode- Code object for player
	 *    
	 * @return Boolean - True if player has won, false otherwise   
	 */
	public Boolean placePegs(Code playerCode, Player p1, int turn)
	{
		int black = 0;
		int white = 0;
		boolean[] matched = new boolean[numPegs];
		Arrays.fill(matched, Boolean.FALSE);
		
		for (int i = 0; i < numPegs; i++)
		{
			if(playerCode.colorAt(i) == secretCode.colorAt(i))
			{
				pegs.add(new Peg(true));
				//black++;
				matched[i] = true;
			}
		}
		for(int i = 0; i < numPegs; i++)
		{
			if( playerCode.colorAt(i) != secretCode.colorAt(i))
			{
				for(int j = 0; j< numPegs; j++)
				{
					if(matched[j] == false && playerCode.colorAt(i) == secretCode.colorAt(j))
					{
						pegs.add(new Peg(false));
						matched[j] = true;
						break;
					}
				}
			}
		}
		white = this.getWhitePegs();
		black = this.getBlackPegs();
		resultOut(playerCode,black,white);
		p1.addHistory(playerCode, black, white, (numTurns-turn));
		pegs.clear();
		if(black == numPegs)
			return true;
		else
			return false;
		
		
	}
		
	/**
	 * resultOut Method. Outputs correct number of black and white pegs 
	 * 
	 * @param playerCode - Code object for player
	 * 		  black - number of black pegs
	 * 		  white - number of white pegs
	 *    
	 */
	public void resultOut(Code playerCode, int black, int white)
	{
		String output = "";
		if(debug == true)
		{
			output = "Secret Code: " + secretCode.printCode() + "\n";
		}
		output+=playerCode.printCode();
		output+="-> Result: ";
		if( white == 0 && black == 0){
			output+="No pegs";
		}
		else
		{
			if(black!=0)
			{
				output+=black + " black peg(s) ";
			}
			if(white!=0)
			{
				output+=white + " white peg(s)";
			}
			output+="";
		}
		
		JOptionPane.showMessageDialog(null, output);
	}
	
	/**
	 * getWhitePegs Method. Returns number of white pegs 
	 * 
	 * @return count - number of white pegs
	 *    
	 */
	public int getWhitePegs(){
		int count = 0;
		for(Peg p : pegs)
		{
			if(p.getColor()==false)
			count++;
		}
		
	return count;	
	}

	/**
	 * getBlackPegs Method. Returns number of black pegs 
	 * 
	 * @return count - number of black pegs
	 *    
	 */
	public int getBlackPegs(){
		
		int count = 0;
		for(Peg p : pegs)
		{
			if(p.getColor()==true)
			count++;
		}		
	return count;
	}	
}
