package assignment3;

/**
IllegalGuessException class that handles illegal guesses
Solves EE422C programming assignment #3
@author Biraj Shrestha, Chris Hogue
@section Friday 9-10:30AM
*/
@SuppressWarnings("serial")
public class IllegalGuessException extends RuntimeException
{
	/**
	 * IllegalGuessException Constructor. Default Constructor for IllegalGuessException
	 */
	public IllegalGuessException(){};
	
	/**
	 * IllegalGuessException Constructor. Constructor for IllegalGuessException 
	 * that passes error message to superclass
	 * 
	 * @param message - specific error message
	 * 
	 */
	public IllegalGuessException(String message)
	{
		super(message);
	}

}
