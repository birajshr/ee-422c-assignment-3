package assignment3;
import javax.swing.JOptionPane;

/**
Driver class creates and runs a game
Solves EE422C programming assignment #3
@author Biraj Shrestha, Chris Hogue
@section Friday 9-10:30AM
*/
public class Game {
	
	private int numTurns = 15; 
	private int numPegs = 5;
	Board mastermind;
	Player p1;
	
	public static void main(String args[])
	{
		
		rules();
		String start;	
		do
		{
			start = JOptionPane.showInputDialog(null, "Are you ready to play?\n"
					+ "(Y/N): ");
			
			if(start.equals("Y"))
			{
				/* we chose to not prompt the user for debug mode input because this is 
				   a feature that only clients should use. A normal user is there to simply
				   play the game.
				*/
				Game newGame = new Game(true); 
				newGame.runGame();
			}
			else if(!start.equals("N"))
			{
				JOptionPane.showMessageDialog(null, "INVALID INPUT");
			}
		}
		while(!(start.equals("Y") || start.equals("N")));
		while(start.equals("Y"))
		{
			do
			{
				start = JOptionPane.showInputDialog(null, "Are you ready to play?\n"
						+ "(Y/N): ");
				if(start.equals("Y"))
				{
					Game newGame = new Game(true);
					newGame.runGame();
				}
				
				else if(!start.equals("N"))
				{
					JOptionPane.showMessageDialog(null, "INVALID INPUT");
				}
			}
			while(!(start.equals("Y") || start.equals("N")));
		}
	}
	
	/**
	 * Game costructor. Initializes new Board and Player objects.
	 * 
	 * @param debugMode - if true, the secret code is revealed during each turn.
	 */	
	public Game(boolean debugMode)
	{
		mastermind = new Board(debugMode, numTurns, numPegs);
		p1 = new Player(numTurns);
	}
	
	/**
	 * Cycles through each turn, decrementing afterwards, and checks to see if the player
	 * has won the game. If so, the game is over.     
	 */
	public void runGame()
	{
		while(numTurns > 0)
		{
			mastermind.turn(p1, numTurns);
			if( p1.winner == true)
			{
				JOptionPane.showMessageDialog(null, "You Win!");
				break;
			}
			numTurns--;
			
		}
		if(p1.winner == false)
		{
		JOptionPane.showMessageDialog(null, "You ran out of turns. You lose.");
		}
	}
	
	/**
	 * Outputs the rules of the game Mastermind    
	 */
	public static void rules()
	{
		System.out.println("Welcome to Mastermind.  Here are the rules.");
		System.out.println("This is a text version of the classic board game Mastermind.");
		System.out.println("The computer will think of a secret code. The code consists of 5 colored pegs.");
		System.out.println("The pegs MUST be one of six colors: blue, green, orange, purple, red, or yellow. ");
		System.out.println("A color may appear more than once in the code. You try to guess what colored pegs are in the code and what order they are in.");
		System.out.println("After you make a valid guess the result (feedback) will be displayed.");
		System.out.println("The result consists of a black peg for each peg you have guessed exactly correct (color and position)");
		System.out.println("in your guess.  For each peg in the guess that is the correct color, but is out of position, you get a white peg.");
		System.out.println("For each peg, which is fully incorrect, you get no feedback."); 
		System.out.println("Only the first letter of the color is displayed. B for Blue, R for Red, and so forth.");
		System.out.println("When entering guesses you only need to enter the first character of each color as a capital letter.");
		System.out.println("You have 15 guesses to figure out the secret code or you lose the game.");
		System.out.println("");
	}
}
