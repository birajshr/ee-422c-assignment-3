package assignment3;
import javax.swing.*;

/**
Player class which handles interaction between Game and Player
Solves EE422C programming assignment #3
@author Biraj Shrestha, Chris Hogue
@section Friday 9-10:30AM
*/
public class Player 
{
	boolean winner;
	String[] history;
	String[] codeHistory;
	
	/**
	 * Player Constructor. Creates Player Object that holds codeHistory,history, and winner attributes 
	 * 
	 * @param numTurns - total number of turns for game
	 * 
	 */
	public Player(int numTurns)
	{
		codeHistory = new String[numTurns];
		history = new String[numTurns];
		winner = false;
	}
	
	/**
	 * getInput Method. Returns code version of player's input
	 * 
	 * @param turn - current turn
	 * 		  board - current board being used
	 * 
	 * @return inCode - code version of player's input
	 * 
	 */
	Code getInput (int turn, Board mastermind)
	{
		Code inCode;
		
		String guess = JOptionPane.showInputDialog(null, "You have " + turn + " guess(es) left.\n"
		+ "What is your next guess?\nType in the characters for your guess and press enter.\n"
		+ "Enter guess: ");
		
		
		if(guess.equals("history") || guess.equals("History"))
		{
			displayHistory();
			inCode = getInput(turn, mastermind);
		}		
		else
		{
			inCode = new Code(guess, mastermind);
		}
		
		return inCode;
	
	}

	/**
	 * addHistory Method. Adds the code and result of a turn
	 * 
	 * @param pCode - players's code to be added to history
	 * 		  black - number of black pegs
	 * 		  white - number of white pegs
	 * 		  turn - current turn
	 * 
	 */
	public void addHistory(Code pCode, int black, int white, int turn)
	{
		history[turn] = "Guess: " + pCode.printCode() + " Black Pegs: " + black + " White Pegs " + white;
		codeHistory[turn] = pCode.printCode();
	}
	
	
	/**
	 * displayHistory Method. Outputs the player's entire history for the current game
	 */
	public void displayHistory()
	{
		String output = "";
		int i = 0;
		while((history[i] != null))
		{
			output+="";
			output+="Guess " + (i+1) + " -- ";
			output+=history[i];
			i+=1;
			output+="\n";
		}
		
		JOptionPane.showMessageDialog(null,output);
		
	}
	
	/**
	 * codeExists Method. Checks to see if code already exists in history
	 * 
	 * @param code - code to be checked
	 * 
	 * @return output -  true if code exists in history, false otherwise
	 * 
	 */
	public boolean codeExists(String code)
	{
		boolean output = false;
		
		for(String s : this.codeHistory)
		{
			if(code.equals(s))
				output = true;
		}
		return output;
	}
	
}
