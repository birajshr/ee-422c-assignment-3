package assignment3;

/**
Peg class containing Peg objects that are either black or white
Solves EE422C programming assignment #3
@author Biraj Shrestha, Chris Hogue
@section Friday 9-10:30AM
*/
public class Peg {
	
	private boolean color;	// Black = true     White = false
	
	/**
	 * Peg Constructor. Creates a Peg object with of black of white color 
	 * 
	 * @param b - color of Peg object
	 * 
	 */
	public Peg (boolean b){
		color = b;
	}
	
	/**
	 * getColor Method. Returns color of Peg object 
	 * 
	 * @return this.color - color of current peg object
	 * 
	 */
	public boolean getColor()
	{
		return this.color;
	}
	
}
