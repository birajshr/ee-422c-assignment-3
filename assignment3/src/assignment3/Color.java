package assignment3;

/**
Color class that holds available colors for Code
Solves EE422C programming assignment #2
@author Biraj Shrestha, Chris Hogue 
*/
public class Color 
{	
	private char color;

	/**
	 * Color Constructor. Created color object that holds char c 
	 * 
	 * @param c - The color of the object
	 * 
	 */
	public Color(char c)
	{
		color = c;
	}
		
	/**
	 * getColor Method. Returns the color of the object 
	 * 
	 * @return color - color of the object
	 * 
	 */
	public char getColor()
	{
		return color;
	}
}
