package assignment3;
import java.lang.Math;

import javax.swing.JOptionPane;

/**
Code class that holds game codes
Solves EE422C programming assignment #2
@author Biraj Shrestha, Chris Hogue 
*/
public class Code 
{	
	String val;
	Color[] gameCol;
	
	/**
	 * Code Constructor. Creates an Empty code 
	 * 
	 * @param codeLength - length of code
	 * 		  mastermind - current board being used
	 *   
	 */
	public Code(int codeLength, Board mastermind)
	{
		gameCol = mastermind.gameColors;
		val = "";
		for(int i = 0; i<codeLength; i+=1)
		{
			val+=gameCol[(int)(Math.random() *gameCol.length)].getColor();		
		}	
	}
	
	/**
	 * Code Constructor. Creates an code of String input 
	 * 
	 * @param input - contents of code being created
	 * 		  mastermind - current board being used
	 *   
	 */
	public Code (String input, Board mastermind)
	{
		gameCol = mastermind.gameColors;
		val = input;
	}
		
	/**
	 * valid Method. Check if p1 has inputed a valid code 
	 * 
	 * @param numPegs - correct code length
	 * 		  p1 - Player playing game
	 * 
	 * @return charValid - true if code is valid, false otherwise 
	 * 
	 * @throws IllegalGuessException - throws if Invalid Guess Length, Invalid Color, Previous Guess
	 *  
	 */
	public boolean valid(int numPegs, Player p1)
	{
		try
		{
		
		if(val.length() != numPegs)
		{
			throw new IllegalGuessException("Err: Invalid Guess Length");
		}
		
		for(int i = 0; i<val.length(); i +=1)
		{
			Boolean charValid = false;
			for(int j = 0; j<gameCol.length; j+=1)
			{
				if(val.charAt(i) == gameCol[j].getColor())
				{
					charValid = true;
				}
			}
			if( charValid == false)
			{
				throw new IllegalGuessException("Err: Invalid Color");
			}
		}
		
		if(p1.codeExists(val) == true)
		{
			throw new IllegalGuessException("Err: You have already made that guess before");
		}
		
		}
		catch(IllegalGuessException e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * printCode Method. Returns the string value of the code 
	 * 
	 * @return val - String value of the code
	 * 
	 */
	public String printCode()
	{
		return val;
	}
	
	/**
	 * colorAt Method. Returns the string value of the code 
	 * 
	 * @param i - index of the specified color
	 * 
	 * @return val.charAt(i) - value of specified color
	 * 
	 */
	public char colorAt(int i)
	{
		return val.charAt(i);
	}	
}
