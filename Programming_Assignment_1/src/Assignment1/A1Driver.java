/**
   Classes/Methods to detect Palindromes
   Solves EE422C Programming Assignment #1
   @author Biraj Shrestha
   @version 2.3 2014-09-14
   @eid BS29898
   @Section 16820
   @hours 14
   @logic errors 12
*/


/*Your Header comment block goes here */

package Assignment1;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class A1Driver 
{
	
	public static void main (String args[]) 
	{
		if (args.length != 1) 
		{
			System.err.println ("Error: Incorrect number of command line arguments");
			System.exit(-1);
		}
		processLinesInFile (args[0]);	
	}

	/**
	 * Opens the file specified in String filename, reads each line in it
	 * Invokes parse () on each line in the file, and prints out the  
	 * string with all discovered palindromes in it.
	 * @param filename - the name of the file that needs to be read
	 */
	public static void processLinesInFile (String filename) 
	{ 

		A1Driver myPalFinder = new A1Driver(); 
		try 
		{
			FileReader freader = new FileReader(filename);
			BufferedReader reader = new BufferedReader(freader);
			
			for (String s = reader.readLine(); s != null; s = reader.readLine()) 
			{
				System.out.println("The input string is: " + s);
				String palindromes = myPalFinder.parse(s);
				System.out.println("The Palindromes found: " + palindromes);
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.err.println ("Error: File not found. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		} 
                catch (IOException e) 
		{
			System.err.println ("Error: IO exception. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	
	/**
	 * Parses the inputString to find all Palindromes based on rules specified 
	 * in your assignment write-up. 
	 * 
	 * @param inputString - the String that needs to be parsed to discover palindromes 
	 * 		
	 * @return the String object containing the Palindromes found in the
	 *         inputString    
	 */
	public String parse (String inputString)           
	{ 	
		//String inputString = "RacecarRacecarRacecarRacecar";
		String outputString = inputString.toUpperCase();
		outputString = characterClean(outputString);
		//System.out.println("Cleaned String : " + outputString);
		String [] outputStringMe = allPalindromes(outputString);
		
		/*for(String outputAllOfThem : outputStringMe)
		{
			if(outputAllOfThem == null)
				break;
			System.out.println("All of the Strings : " + outputAllOfThem);
		}*/
		
		
		
		outputString = eliminateBad(outputStringMe);
		
		return outputString;
	}
	
		
	/**
	 * Parses String[] allStrings and eliminates bad Palindromes from Array 
	 * 
	 * @param allStrings - Array of Strings that need to be parsed to eliminate bad Palindromes 
	 * 		
	 * @return String object of all valid Palindromes  
	 */	
	public String eliminateBad(String[] allStrings)
	{
		String[] badPali = badPalindromes(allStrings);
		
		
		for(int x = 0; x<allStrings.length;x++)
		{
			if(allStrings[x] == null)
				break;
			
			for(int y = 0; y<badPali.length;y++)
			{
				if(badPali[y] == null)
					break;
				
				if(allStrings[x].equals(badPali[y]))
				{
					allStrings[x] = "biraj";
				}	
			}			
		}
		
		int newOutputIndex = 0;
		String[] newOutputString = new String[400];
		Boolean found = false;
		
		for(String allStringArray : allStrings)
		{
			if(allStringArray == null)
				break;
			
			if(allStringArray != "biraj")
			{
				for(int x = 0 ; x<=newOutputIndex ; x++)
				{
					if(allStringArray.equals(newOutputString[x]))
					found = true;	
				}
			
			if(found == false)
				{
					newOutputString[newOutputIndex] = allStringArray;
					newOutputIndex++;
				}
			}	
		}
 
		
		allStrings = newOutputString;
		allStrings = sortPalindrome(allStrings);
		
		String returnThis = new String();
		for(String addString : allStrings)
		{
			if(addString == null)
				break;
				returnThis = returnThis + " " + addString;
		}
		
		
		
		
		return returnThis;
	}
		
	
	/**
	 * Parses String[] inputString and sorts Strings in lexicographical order 
	 * 
	 * @param inputStrings - Array of Strings that needs to be sorted in lexicographical order 
	 * 		
	 * @return Array of strings sorted in lexicographical order
	 */	
	public String[] sortPalindrome(String[] inputString)
	{
		String[] returnedString = inputString;
		Boolean arraySwap = true;
		int x = 0;
		String tempString;
		
		while(arraySwap == true)
		{	
			arraySwap = false;
			
			while(returnedString[x + 1] != null)
				{
					if(returnedString[x+1].compareTo(returnedString[x]) < 0)
					{
						tempString = returnedString[x];
						returnedString[x] = returnedString[x+1];
						returnedString[x+1] = tempString;
						arraySwap = true;
					}
					
					x++;
			
				}
			x = 0;
		}
		
		
		return returnedString;
		
	}
	

	/**
	 * Parses String[] allStrings and find all bad Palindromes
	 * 
	 * @param allStrings - Array of Strings that need to be parsed to find bad Palindromes 
	 * 		
	 * @return String Array object of all bad Palindromes  
	 */	
	public String[] badPalindromes(String[] allStrings)
	{
		int badStringIndex = 0;
		int startIndex;
		int nextEndIndex;
		int offset;
	
		String[] badStrings = new String[400];
		
		for(String checkThis : allStrings)
		{
			if(checkThis == null)
			break;
			
			startIndex = 0;
			nextEndIndex = checkThis.length() - 1;
			offset = 1;
			
			
			
			while(startIndex + offset < nextEndIndex - offset)
			{
			
			if(checkIfPalindrome(checkThis, startIndex + offset , nextEndIndex - offset) == true)
			{
				badStrings[badStringIndex] = checkThis.substring(startIndex + offset, nextEndIndex - offset + 1);
				badStringIndex++;
			}
			
			offset++;
			}
			
		}
		
		
		for(String checkMe : badStrings)
		{
			if(checkMe == null)
				break;
			
			//System.out.println("BAD STRING: " + checkMe);
		}
			
		
		return badStrings;
		
		
	}
	
	
	/**
	 * Parses inputString and deletes non-valid characters
	 * 
	 * @param inputString - String object that needs to cleaned of non-valid characters
	 * 		
	 * @return String Array object of cleaned inputString
	 */	
	public String characterClean(String inputString)
	{
		
		StringBuilder returnedString = new StringBuilder(inputString);
		
		Boolean cleanLoop = false;
		
		while(cleanLoop == false)
		{
			
			cleanLoop = true;
		
			for( int x = returnedString.length() - 1 ; x>=0  ; x--)
			{
				if(returnedString.charAt(x) < 'A' || returnedString.charAt(x)>'Z')
				{	
					returnedString.deleteCharAt(x);
					cleanLoop = false;
				}
			}
		}
		
		return returnedString.toString();
	}
		
	
	/**
	 * Parses inputString and find/stores all possible Palindromes
	 * 
	 * @param inputString - String object used to find all possible Palindromes
	 * 		
	 * @return String Array object of all possible Palindromes  
	 */	
	public String[] allPalindromes(String inputString)
	{
		String[] allOutputs = new String [400];
		int numStringArray = 0;
		int beginningIndex = 0;
		int tempBeginningIndex = 0;
		int tempNextIndex  = 2;
		int endingIndex = inputString.length() - 1;
		
		while(beginningIndex < endingIndex)
		{
			tempBeginningIndex = beginningIndex;
			tempNextIndex = endingIndex;
			
			if(tempNextIndex<tempBeginningIndex + 2)
				break;
			
			
			while(tempBeginningIndex < tempNextIndex)
			{
			
			while(inputString.charAt(tempBeginningIndex) != inputString.charAt(tempNextIndex))
			{
				tempNextIndex--;
				if(tempNextIndex<tempBeginningIndex + 2)
					break;
			}
			
			
			if(tempNextIndex>=tempBeginningIndex + 2)
			{
				if(checkIfPalindrome(inputString,tempBeginningIndex, tempNextIndex) == true)
				{
					allOutputs[numStringArray] = new String(inputString.substring(tempBeginningIndex, tempNextIndex + 1));
					//System.out.println("ONE OF TheM : " + allOutputs[numStringArray]);
					numStringArray++;
				}
			}
			
			
			tempNextIndex--;
			
			}
			beginningIndex++;
		}
		
		if(numStringArray == 0)
			allOutputs[0] = "None Found";
		
		return allOutputs;
	}
		
		
	/**
	 * Determines if inputString holds a Palindrome
	 * 
	 * @param inputString - String object utilized to show if Palindrome exists 
	 * 		  startIndex - Beginning index for inputString
	 * 		  endIndex - Ending index for inputString			
	 * @return Boolean value whether inputString holds a Palindrome  
	 */	
	public Boolean checkIfPalindrome(String inputString, int startIndex, int endIndex)
	{
		int start = startIndex;
		int end = endIndex;
		Boolean returnedValue = true;
		
		while((start + 1) < (end - 1))
		{
			if(inputString.charAt(start + 1) != inputString.charAt(end - 1))
				returnedValue = false;
			start++;
			end--;
		}
		return returnedValue;
	}
	
	
}