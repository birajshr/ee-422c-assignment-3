package assighnment2;


import javax.swing.*;

public class Player {
	boolean winner;
	String[] history;
	String[] codeHistory;
	public Player(int numTurns)
	{
		codeHistory = new String[numTurns];
		history = new String[numTurns];
		winner = false;
	}
	
	Code getInput (int turn, Board mastermind)
	{
		Code inCode;
		
		String guess = JOptionPane.showInputDialog(null, "You have " + turn + " guess(es) left.\n"
		+ "What is your next guess?\nType in the characters for your guess and press enter.\n"
		+ "Enter guess: ");
		
		
		if(guess.equals("history") || guess.equals("History"))
		{
			displayHistory();
			inCode = getInput(turn, mastermind);
		}		
		else
		{
			inCode = new Code(guess, mastermind);
		}
		
		return inCode;
	
	}
	
	public void addHistory(Code pCode, int black, int white, int turn)
	{
		history[turn] = "Guess: " + pCode.printCode() + " Black Pegs: " + black + " White Pegs " + white;
		codeHistory[turn] = pCode.printCode();
	}
	
	public void displayHistory()
	{
		String output = "";
		int i = 0;
		while((history[i] != null))
		{
			output+="";
			output+="Guess " + (i+1) + " -- ";
			output+=history[i];
			i+=1;
			output+="\n";
		}
		
		JOptionPane.showMessageDialog(null,output);
		
	}
	
	public boolean codeExists(String code)
	{
		boolean output = false;
		
		for(String s : this.codeHistory)
		{
			if(code.equals(s))
				output = true;
		}
		return output;
	}
	
	

}
