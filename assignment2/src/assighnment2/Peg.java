package assighnment2;

public class Peg {
	
	private boolean color;	// Black = true     White = false
	
	public Peg (boolean b){
		color = b;
	}
	
	public boolean getColor()
	{
		return this.color;
	}
	
}
