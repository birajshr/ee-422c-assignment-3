package assighnment2;

import java.lang.Math;

import javax.swing.JOptionPane;

public class Code {
	
	String val;
	Color[] gameCol;
	//char[] colors = {'B','G','O','P','R','Y'};

	public Code(int codeLength, Board mastermind)
	{
		gameCol = mastermind.gameColors;
		val = "";
		for(int i = 0; i<codeLength; i+=1)
		{
			val+=gameCol[(int)(Math.random() *gameCol.length)].getColor();
			
		}
		
		
	}
	
	public Code (String input, Board mastermind)
	{
		gameCol = mastermind.gameColors;
		val = input;
	}
	
	public boolean valid(int numPegs, Player p1)
	{
		if(val.length() != numPegs)
		{
			JOptionPane.showMessageDialog(null, "INVALID INPUT");
			return false;
		}
		
		for(int i = 0; i<val.length(); i +=1)
		{
			Boolean charValid = false;
			for(int j = 0; j<gameCol.length; j+=1)
			{
				if(val.charAt(i) == gameCol[j].getColor())
				{
					charValid = true;
				}
			}
			if( charValid == false)
			{
				JOptionPane.showMessageDialog(null, "INVALID INPUT");
				return false;
			}
		}
		
		if(p1.codeExists(val) == true)
		{
			JOptionPane.showMessageDialog(null, "You have already made that guess before");
			return false;
		}
		
		return true;
	}
	
	public String printCode()
	{
		return val;
	}
	
	public char colorAt(int i)
	{
		return val.charAt(i);
	}
}
